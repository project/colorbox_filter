<?php

namespace Drupal\colorbox_filter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\FormStateInterface;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a filter to link images derivates to source (original) image.
 *
 * @Filter(
 *   id = "filter_image_link_to_colorbox",
 *   title = @Translation("Link with colorbox"),
 *   description = @Translation("Colorbox"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class FilterImageLinkToColorbox extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a \Drupal\editor\Plugin\Filter\EditorFileReference object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   An entity manager object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    $result = new FilterProcessResult($text);

    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);


    // Get settings form.
    $excludeClass = isset($this->settings['excludeClass']) ? $this->settings['excludeClass'] : NULL;

    $includeClass = isset($this->settings['includeClass']) ? $this->settings['includeClass'] : NULL;


    $allowExternal = isset($this->settings['allowExternal']) ? $this->settings['allowExternal'] : FALSE;

    $colorbox = isset($this->settings['colorbox']) ? $this->settings['colorbox'] : 1;

    $classLink = isset($this->settings['classLink']) ? $this->settings['classLink'] . ' colorbox' : 'colorbox';
    $relLink = isset($this->settings['relLink']) ? $this->settings['relLink'] : '';



    /** @var \DOMNode $node */
    foreach ($xpath->query('//img') as $node) {


      if ($node->parentNode->nodeName !== 'a') {


        // Read the data-align attribute's value, then delete it.
        $width = $node->getAttribute('width');
        $height = $node->getAttribute('height');
        $src = $node->getAttribute('src');
        $class = $node->getAttribute('class');
        $rel = $node->getAttribute('rel');

        $external = UrlHelper::isExternal($src);



        $matchExcludeClass = false;
        if($excludeClass) {
          if(!preg_match('/(' . preg_replace('/ /', '|', $excludeClass) . ')/', $class)){
            $matchExcludeClass = true;
          }
        }

        // exclude image with class and external image
        if (!$matchExcludeClass AND ($allowExternal OR (!$allowExternal and !$external))) {

          //
          $fullPath = $src;
          if (!$external) {
            Global $base_root;
            $fullPath = $base_root . $src;
          }

          $imageSize = getimagesize($fullPath);

          $resizedImage = FALSE;
          if ($imageSize[0] != $width and $imageSize[1] = $height) {
            $resizedImage = TRUE;
          }


          $matchIncludeClass = false;
          if($includeClass) {
            if(!preg_match('/(' . preg_replace('/ /', '|', $includeClass) . ')/', $class)){
              $matchIncludeClass = true;
            }
          }

          if (($resizedImage and $colorbox == 1) OR $matchIncludeClass OR $colorbox == 0) {


            // Add class and rel colorbox.
            $rel = ($rel) ? $rel . ' ' . $relLink : $relLink;
            $class = ($class) ? $class . ' ' . $classLink : $classLink;


            if ($width || $height) {

              /** @var \DOMNode $element */
              $element = $dom->createElement('a');
              $element->setAttribute('href', $src);


              $element->setAttribute('class', $class);

              if ($rel) {
                $element->setAttribute('rel', $rel);
              }

              $node->parentNode->replaceChild($element, $node);
              $element->appendChild($node);
            }
          }
        }
      }

    }
    $result->setProcessedText(Html::serialize($dom));


    // Attach the Colorbox JS and CSS.
    $lib = $this->getConfigColorbox();
    $result->setAttachments($lib);



    return $result;
  }

  /**
   * Function Setting form.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {


    $form['colorbox'] = [
      '#type' => 'radios',
      '#title' => $this->t('Colorbox on'),
      '#default_value' => isset($this->settings['colorbox']) ? $this->settings['colorbox'] : 1,
      '#options' => [
        0 => $this->t('all image'),
        1 => $this->t('only resized image'),
        2 => $this->t('only with rules'),
      ],
      '#description' => $this->t('Except image has already link'),
    ];


    $form['allowExternal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include external image'),
      '#default_value' => isset($this->settings['allowExternal']) ? $this->settings['allowExternal'] : 0,
    ];

    $form['excludeClass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Exclude colorbox with class'),
      '#description' => $this->t('Seperated by a space and without point'),
      '#default_value' => isset($this->settings['excludeClass']) ? $this->settings['excludeClass'] : '',
    ];
    $form['includeClass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add colorbox with class'),
      '#description' => $this->t('Seperated by a space and without point'),
      '#default_value' => isset($this->settings['includeClass']) ? $this->settings['includeClass'] : '',
    ];

    $form['classLink'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add class attribute on link'),
      '#default_value' => isset($this->settings['classLink']) ? $this->settings['classLink'] : '',
    ];
    $form['relLink'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add rel attribute on link'),
      '#default_value' => isset($this->settings['relLink']) ? $this->settings['relLink'] : '',
    ];


    $form['excludeClass']['#element_validate'][] = [$this, 'validateClass'];
    $form['includeClass']['#element_validate'][] = [$this, 'validateClass'];
    $form['classLink']['#element_validate'][] = [$this, 'validateClass'];

    return $form;
  }

  /**
   * Ensure values entered for color hex values contain no unsafe characters.
   *
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateClass(array $element, FormStateInterface $form_state) {

    if ($element['#value']) {
      if (preg_match('/([^a-zA-z0-9\-\_ ])/i', $element['#value'])) {
        $form_state->setError($element, 'Only valid class values are allowed (a-zA-Z0-9_-). Please check your settings and try again.');
      }
    }

  }

  /**
   * Get settings colorbox and attachment.
   */
  private function getConfigColorbox() {
    $settings = \Drupal::config('colorbox.settings');
    $lib = [];


    if ($settings->get('custom.activate')) {
      $js_settings = array(
        'transition' => $settings->get('custom.transition_type'),
        'speed' => $settings->get('custom.transition_speed'),
        'opacity' => $settings->get('custom.opacity'),
        'slideshow' => $settings->get('custom.slideshow.slideshow') ? TRUE : FALSE,
        'slideshowAuto' => $settings->get('custom.slideshow.auto') ? TRUE : FALSE,
        'slideshowSpeed' => $settings->get('custom.slideshow.speed'),
        'slideshowStart' => $settings->get('custom.slideshow.text_start'),
        'slideshowStop' => $settings->get('custom.slideshow.text_stop'),
        'current' => $settings->get('custom.text_current'),
        'previous' => $settings->get('custom.text_previous'),
        'next' => $settings->get('custom.text_next'),
        'close' => $settings->get('custom.text_close'),
        'overlayClose' => $settings->get('custom.overlayclose') ? TRUE : FALSE,
        'returnFocus' => $settings->get('custom.returnfocus') ? TRUE : FALSE,
        'maxWidth' => $settings->get('custom.maxwidth'),
        'maxHeight' => $settings->get('custom.maxheight'),
        'initialWidth' => $settings->get('custom.initialwidth'),
        'initialHeight' => $settings->get('custom.initialheight'),
        'fixed' => $settings->get('custom.fixed') ? TRUE : FALSE,
        'scrolling' => $settings->get('custom.scrolling') ? TRUE : FALSE,
        'mobiledetect' => $settings->get('advanced.mobile_detect') ? TRUE : FALSE,
        'mobiledevicewidth' => $settings->get('advanced.mobile_device_width'),
      );
    }
    else {
      $js_settings = array(
        'opacity' => '0.85',
        'current' => $this->t('{current} of {total}'),
        'previous' => $this->t('« Prev'),
        'next' => $this->t('Next »'),
        'close' => $this->t('Close'),
        'maxWidth' => '98%',
        'maxHeight' => '98%',
        'fixed' => TRUE,
        'mobiledetect' => $settings->get('advanced.mobile_detect') ? TRUE : FALSE,
        'mobiledevicewidth' => $settings->get('advanced.mobile_device_width'),
      );
    }

    $style = $settings->get('custom.style');

    // Give other modules the possibility to override Colorbox
    // settings and style.
    $this->moduleHandler->alter('colorbox_settings', $js_settings, $style);

    $lib['drupalSettings']['colorbox'] = $js_settings;

    // Add and initialise the Colorbox plugin.
    if ($settings->get('advanced.compression_type') == 'minified') {
      $lib['library'][] = 'colorbox/colorbox';
    }
    else {
      $lib['library'][] = 'colorbox/colorbox-dev';
    }

    // Add JS and CSS based on selected style.
    if ($style != 'none') {
      $lib['library'][] = "colorbox/$style";
    }

    return $lib;
  }

}
